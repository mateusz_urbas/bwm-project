import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Rental } from './rental.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RentalService {

  constructor(private http: HttpClient) {}

  public getRentalById(rentalId: string): Observable<Rental> {
    return <Observable<Rental>>this.http.get('/api/v1/rentals/' + rentalId);
  }

  public getRentals(): Observable<Rental[]> {
    return <Observable<Rental[]>>this.http.get('/api/v1/rentals');
  }

  public getRentalsByCity(city: string): Observable<Rental[]> {
    return <Observable<Rental[]>>this.http.get(`/api/v1/rentals?city=${city}`);
  }

  public createRental(rental: Rental): Observable<Rental> {
    return <Observable<Rental>>this.http.post('/api/v1/rentals', rental);
  }

  public getUserRentals(): Observable<Rental[]> {
    return <Observable<Rental[]>>this.http.get('/api/v1/rentals/manage');
  }

  public deleteRental(rentalId: string): Observable<Rental> {
    return <Observable<Rental>>this.http.delete(`/api/v1/rentals/${rentalId}`);
  }
}
